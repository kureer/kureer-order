package id.ac.ui.cs.advrog.kureer.service;

import com.fasterxml.jackson.databind.JsonNode;
import id.ac.ui.cs.advrog.kureer.core.Address;
import id.ac.ui.cs.advrog.kureer.core.Order;
import id.ac.ui.cs.advrog.kureer.model.dto.OrderAddressDto;
import id.ac.ui.cs.advrog.kureer.repository.AddressRepository;
import id.ac.ui.cs.advrog.kureer.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;

@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private AddressRepository addressRepository;

    @Override
    public Order save(OrderAddressDto orderAddressDto) {
        Address senderAddress = new Address(orderAddressDto.getSenderAlamat(),
                orderAddressDto.getSenderDesaKelurahan(), orderAddressDto.getSenderKecamatan(),
                orderAddressDto.getSenderKotaKabupaten(), orderAddressDto.getSenderProvinsi(),
                orderAddressDto.getSenderKodePos());
        addressRepository.save(senderAddress);

        Address receiverAddress = new Address(orderAddressDto.getReceiverAlamat(),
                orderAddressDto.getReceiverDesaKelurahan(), orderAddressDto.getReceiverKecamatan(),
                orderAddressDto.getReceiverKotaKabupaten(), orderAddressDto.getReceiverProvinsi(),
                orderAddressDto.getReceiverKodePos());
        addressRepository.save(receiverAddress);

        Order order = new Order(orderAddressDto.getUserId(), senderAddress, receiverAddress,
                orderAddressDto.getReceiverName(), orderAddressDto.getReceiverPhoneNumber(),
                orderAddressDto.getType(), orderAddressDto.getWeight(), orderAddressDto.getLength(),
                orderAddressDto.getWidth(), orderAddressDto.getHeight());
        orderRepository.save(order);

        CompletableFuture.runAsync(() -> updateCost(order, senderAddress.getKotaKabupaten(),
                receiverAddress.getKotaKabupaten(), order.getWeight()));

        return order;
    }

    @Override
    public List<Order> getAllOrder() {
        return orderRepository.findAll();
    }

    @Override
    public List<Order> getListOrderByUserId(Long userId) {
        return orderRepository.findOrdersByUserId(userId);
    }

    @Override
    public Order getOrderById(Long id) {
        return orderRepository.findById(id).orElse(null);
    }

    @Override
    public Order confirmPayment(Long id, Long extraCost) {
        try {
            Order order = orderRepository.getById(id);
            order.setCost(order.getCost() + extraCost);
            order.setPayed(true);
            orderRepository.save(order);
            return order;
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public void updateCost(Order order, String senderKotaKabupaten,
                           String receiverKotaKabupaten, Long orderWeight) {

        String senderCityId = null;
        String receiverCityId = null;

        HttpHeaders headers = new HttpHeaders();
        headers.set("key", "61bcc995f81f2de6b1dbfb343c5cd542");
        HttpEntity<Void> requestEntity = new HttpEntity<>(headers);

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<JsonNode> response = restTemplate.exchange(
                "https://api.rajaongkir.com/starter/city",
                HttpMethod.GET,
                requestEntity,
                JsonNode.class);

        JsonNode map = response.getBody();
        assert map != null;
        JsonNode result = map.get("rajaongkir").get("results");

        for (JsonNode detailsCity: result) {
            if (senderCityId == null) {
                if (checkKotaKabupaten(senderKotaKabupaten, detailsCity)) {
                    senderCityId = detailsCity.get("city_id").asText();
                }
            }
            if (receiverCityId == null) {
                if (checkKotaKabupaten(receiverKotaKabupaten, detailsCity)) {
                    receiverCityId = detailsCity.get("city_id").asText();
                }
            }
            if (senderCityId != null && receiverCityId != null) {
                break;
            }
        }

        Long cost = 15000L;
        if (senderCityId != null && receiverCityId != null) {
            cost += calculateCost(senderCityId, receiverCityId, orderWeight);
        } else {
            cost += 20000L;
        }

        order.setCost(cost);
        orderRepository.save(order);
    }

    @Override
    public Long calculateCost(String senderCityId, String receiverCityId, Long orderWeight) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("key", "61bcc995f81f2de6b1dbfb343c5cd542");
        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("origin", senderCityId);
        map.add("destination", receiverCityId);
        map.add("weight", String.valueOf(orderWeight));
        map.add("courier", "jne");

        HttpEntity<MultiValueMap<String, String>> requestEntity = new HttpEntity<>(map, headers);

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<JsonNode> response = restTemplate.exchange(
                "https://api.rajaongkir.com/starter/cost",
                HttpMethod.POST,
                requestEntity,
                JsonNode.class);

        JsonNode result = Objects.requireNonNull(response.getBody()).get("rajaongkir");
        result = result.get("results").get(0).get("costs");

        long costOrder = 0L;
        for (JsonNode cost: result) {
            long costService = cost.get("cost").get(0).get("value").asLong();
            if (costOrder == 0L || costService < costOrder) {
                costOrder = costService;
            }
        }

        return Math.max(costOrder, 15000L);
    }

    @Override
    public boolean checkKotaKabupaten(String kotaKabupaten, JsonNode detailsCity) {
        if (kotaKabupaten.startsWith("KAB.")
                && detailsCity.get("type").asText().equals("Kabupaten")) {
            if (kotaKabupaten.toLowerCase()
                    .endsWith(detailsCity.get("city_name").asText().toLowerCase())) {
                return true;
            }
        }
        if (kotaKabupaten.startsWith("KOTA")
                && detailsCity.get("type").asText().equals("Kota")) {
            return kotaKabupaten.toLowerCase()
                    .endsWith(detailsCity.get("city_name").asText().toLowerCase());
        }
        return false;
    }

}
