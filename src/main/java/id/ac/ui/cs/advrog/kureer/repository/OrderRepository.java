package id.ac.ui.cs.advrog.kureer.repository;

import id.ac.ui.cs.advrog.kureer.core.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long>  {
    List<Order> findOrdersByUserId(Long userId);

}
