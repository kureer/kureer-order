package id.ac.ui.cs.advrog.kureer.core;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "address")
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "alamat")
    private String alamat;

    @Column(name = "desa_kelurahan")
    private String desaKelurahan;

    @Column(name = "kecamatan")
    private String kecamatan;

    @Column(name = "kota_kabupaten")
    private String kotaKabupaten;

    @Column(name = "provinsi")
    private String provinsi;

    @Column(name = "kode_pos")
    private String kodePos;

    public Address(String alamat, String desaKelurahan, String kecamatan,
                   String kotaKabupaten, String provinsi, String kodePos) {
        this.alamat = alamat;
        this.desaKelurahan = desaKelurahan;
        this.kecamatan = kecamatan;
        this.kotaKabupaten = kotaKabupaten;
        this.provinsi = provinsi;
        this.kodePos = kodePos;
    }
}
