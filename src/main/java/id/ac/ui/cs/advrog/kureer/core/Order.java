package id.ac.ui.cs.advrog.kureer.core;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import java.util.Date;
import javax.persistence.*;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "orders")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "create_date", updatable = false)
    private Date createDate;

    @Column(name = "user_id")
    private Long userId;

    @ManyToOne
    @JoinColumn(name = "sender_address_id", referencedColumnName = "id")
    private Address senderAddress;

    @Column(name = "receiver_name")
    private String receiverName;

    @Column(name = "receiver_phone_number")
    private String receiverPhoneNumber;

    @ManyToOne
    @JoinColumn(name = "receiver_address_id", referencedColumnName = "id")
    private Address receiverAddress;

    @Column(name = "type")
    private String type;

    @Column(name = "weight")
    private Long weight;

    @Column(name = "length")
    private Long length;

    @Column(name = "width")
    private Long width;

    @Column(name = "height")
    private Long height;

    @Column(name = "cost")
    private Long cost;

    @Column(name = "payed")
    private boolean payed;

    public Order(Long userId, Address senderAddress, Address receiverAddress,
                 String receiverName, String receiverPhoneNumber, String type,
                 Long weight, Long length, Long width, Long height) {
        this.userId = userId;
        this.senderAddress = senderAddress;
        this.receiverAddress = receiverAddress;
        this.receiverName = receiverName;
        this.receiverPhoneNumber = receiverPhoneNumber;
        this.type = type;
        this.weight = weight;
        this.length = length;
        this.width = width;
        this.height = height;
    }
}
