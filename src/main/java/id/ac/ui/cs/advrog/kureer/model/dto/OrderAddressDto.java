package id.ac.ui.cs.advrog.kureer.model.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class OrderAddressDto {
    private Long userId;
    private String receiverName;
    private String receiverPhoneNumber;
    private String type;
    private Long weight;
    private Long length;
    private Long width;
    private Long height;
    private String senderAlamat;
    private String senderDesaKelurahan;
    private String senderKecamatan;
    private String senderKotaKabupaten;
    private String senderProvinsi;
    private String senderKodePos;
    private String receiverAlamat;
    private String receiverDesaKelurahan;
    private String receiverKecamatan;
    private String receiverKotaKabupaten;
    private String receiverProvinsi;
    private String receiverKodePos;

}
