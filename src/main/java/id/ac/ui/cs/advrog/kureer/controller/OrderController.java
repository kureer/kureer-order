package id.ac.ui.cs.advrog.kureer.controller;

import id.ac.ui.cs.advrog.kureer.core.Order;
import id.ac.ui.cs.advrog.kureer.model.dto.OrderAddressDto;
import id.ac.ui.cs.advrog.kureer.service.OrderServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderServiceImpl orderService;

    @GetMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Iterable<Order>> getListOrder(
            @RequestParam(required = false) Long userId) {
        if (userId != null) {
            return ResponseEntity.ok(orderService.getListOrderByUserId(userId));
        }
        return ResponseEntity.ok(orderService.getAllOrder());
    }

    @GetMapping(path = "/{orderId}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Order> getOrder(@PathVariable(value = "orderId") Long orderId) {
        return ResponseEntity.ok(orderService.getOrderById(orderId));
    }

    @GetMapping(path = "/{orderId}/confirm-payment", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Order> confirmPayment(
            @PathVariable(value = "orderId") Long orderId,
            @RequestParam Long extraCost) {
        return ResponseEntity.ok(orderService.confirmPayment(orderId, extraCost));
    }

    @PostMapping(produces = {"application/json"})
    @ResponseBody
    public ResponseEntity<Order> postOrder(@RequestBody OrderAddressDto orderAddressDto) {
        return ResponseEntity.ok(orderService.save(orderAddressDto));
    }

}