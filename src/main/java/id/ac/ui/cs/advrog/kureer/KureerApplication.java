package id.ac.ui.cs.advrog.kureer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KureerApplication {

    public static void main(String[] args) {
        SpringApplication.run(KureerApplication.class, args);
    }

}
