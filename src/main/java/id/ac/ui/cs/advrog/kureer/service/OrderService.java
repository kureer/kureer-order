package id.ac.ui.cs.advrog.kureer.service;

import com.fasterxml.jackson.databind.JsonNode;
import id.ac.ui.cs.advrog.kureer.core.Order;
import id.ac.ui.cs.advrog.kureer.model.dto.OrderAddressDto;

import java.util.List;

public interface OrderService {
    Order save(OrderAddressDto orderAddressDto);

    List<Order> getAllOrder();

    List<Order> getListOrderByUserId(Long userId);

    Order getOrderById(Long id);

    Order confirmPayment(Long orderId, Long extraCost);

    void updateCost(Order order, String senderKotaKabupaten,
                    String receiverKotaKabupaten, Long orderWeight);

    Long calculateCost(String senderCityId, String receiverCityId, Long orderWeight);

    boolean checkKotaKabupaten(String kotaKabupaten, JsonNode detailsCity);
}
