package id.ac.ui.cs.advrog.kureer.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.fasterxml.jackson.databind.ObjectMapper;
import id.ac.ui.cs.advrog.kureer.core.Order;
import id.ac.ui.cs.advrog.kureer.model.dto.OrderAddressDto;
import id.ac.ui.cs.advrog.kureer.service.OrderServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

@WebMvcTest
public class OrderControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private OrderServiceImpl orderService;

    private Order order;

    private OrderAddressDto orderAddressDto;

    @BeforeEach
    void setUp() {
        order = new Order();
        order.setId(0L);

        orderAddressDto = new OrderAddressDto();
    }

    @Test
    public void testGetOrderById() throws Exception {
        when(orderService.getOrderById(0L)).thenReturn(order);

        mockMvc.perform(get("/order/0").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id").value(0L))
                .andExpect(jsonPath("$.payed").value(false));
    }

    @Test
    public void testGetListOrder() throws Exception {
        List<Order> orderList = List.of(order);
        when(orderService.getAllOrder()).thenReturn(orderList);

        mockMvc.perform(get("/order").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id").value(0L))
                .andExpect(jsonPath("$[0].payed").value(false));
    }

    @Test
    public void testGetListOrderByUserId() throws Exception {
        List<Order> orderList = List.of(order);
        when(orderService.getListOrderByUserId(0L)).thenReturn(orderList);

        mockMvc.perform(get("/order")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("userId", "0"))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id").value(0L))
                .andExpect(jsonPath("$[0].payed").value(false));
    }

    @Test
    public void testPostOrder() throws Exception {
        when(orderService.save(orderAddressDto)).thenReturn(order);

        mockMvc.perform(post("/order")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(orderAddressDto)))
                .andExpect(status().isOk());
    }

    @Test
    public void testConfirmPaymentOrder() throws Exception {
        when(orderService.confirmPayment(0L, 1000L)).thenReturn(order);

        mockMvc.perform(get("/order/0/confirm-payment")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("extraCost", "1000"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(0L));
    }
}
