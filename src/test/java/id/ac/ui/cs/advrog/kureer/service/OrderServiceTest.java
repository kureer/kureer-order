package id.ac.ui.cs.advrog.kureer.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import id.ac.ui.cs.advrog.kureer.core.Address;
import id.ac.ui.cs.advrog.kureer.core.Order;
import id.ac.ui.cs.advrog.kureer.model.dto.OrderAddressDto;
import id.ac.ui.cs.advrog.kureer.repository.AddressRepository;
import id.ac.ui.cs.advrog.kureer.repository.OrderRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class OrderServiceTest {

    @Mock
    private OrderRepository orderRepository;

    @Mock
    private AddressRepository addressRepository;

    @InjectMocks
    private OrderServiceImpl orderService;

    private Order order;

    private OrderAddressDto orderAddressDto;

    @BeforeEach
    void setUp() {
        order = new Order();
        orderAddressDto = new OrderAddressDto();
        orderAddressDto.setUserId(0L);
    }

    @Test
    public void testSaveOrder() {
        orderService.save(orderAddressDto);
        verify(addressRepository, times(2)).save(any(Address.class));
        verify(orderRepository, times(1)).save(any(Order.class));
    }

    @Test
    public void testGetAllOrder() {
        List<Order> orders = List.of(order);
        when(orderRepository.findAll()).thenReturn(orders);

        List<Order> orderDummy = orderService.getAllOrder();
        verify(orderRepository, times(1)).findAll();
        assertEquals(orders, orderDummy);
    }

    @Test
    public void testGetListOrderByUserId() {
        List<Order> orders = List.of(order);
        when(orderRepository.findOrdersByUserId(0L)).thenReturn(orders);

        List<Order> orderDummy = orderService.getListOrderByUserId(0L);
        verify(orderRepository, times(1)).findOrdersByUserId(any(Long.class));
        assertEquals(orders, orderDummy);
    }

    @Test
    public void testGetOrderById() {
        when(orderRepository.findById(0L)).thenReturn(Optional.ofNullable(order));
        Order orderDummy = orderService.getOrderById(0L);

        verify(orderRepository, times(1)).findById(any(Long.class));
        assertEquals(order, orderDummy);
    }

    @Test
    public void testConfirmPaymentOrderExist() {
        order.setCost(10000L);
        when(orderRepository.getById(0L)).thenReturn(order);
        Order orderDummy = orderService.confirmPayment(0L, 10000L);

        verify(orderRepository, times(1)).getById(any(Long.class));
        verify(orderRepository, times(1)).save(any(Order.class));
        assertEquals(order, orderDummy);
    }

    @Test
    public void testConfirmPaymentOrderNonExist() {
        Order orderDummy = orderService.confirmPayment(0L, 10000L);

        verify(orderRepository, times(1)).getById(any(Long.class));
        assertNull(orderDummy);
    }

    @Test
    public void testUpdateCostDefault() {
        orderService.updateCost(order, "KOTA XYZ",
                "KAB. ABC", 1000L);
        verify(orderRepository, times(1)).save(any(Order.class));
    }

    @Test
    public void testUpdateCost() {
        orderService.updateCost(order, "KOTA DEPOK",
                "KAB. BREBES", 1000L);
        verify(orderRepository, times(1)).save(any(Order.class));
    }

    @Test
    public void testCalculateCost() {
        Long cost = orderService.calculateCost("501", "114", 1000L);
        assertTrue(cost >= 15000L);
    }
}
