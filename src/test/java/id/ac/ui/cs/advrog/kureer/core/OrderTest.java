package id.ac.ui.cs.advrog.kureer.core;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;
import java.util.Date;

public class OrderTest {

    private Class<?> orderClass;

    private Order order;

    private Address senderAddress;

    private Address receiverAddress;

    private Date date;

    @BeforeEach
    public void setUp() throws Exception {
        orderClass = Class.forName("id.ac.ui.cs.advrog.kureer.core.Order");
        senderAddress = new Address();
        receiverAddress = new Address();
        date = new Date();
        order = new Order(0L, senderAddress, receiverAddress,"Dummy",
                "081234567890", "Pakaian",
                1000L, 100L, 50L, 20L);
        order.setId(-1L);
        order.setCreateDate(date);
        order.setCost(100000L);
    }

    @Test
    public void testOrderIsConcreteClass() {
        assertFalse(Modifier.isAbstract(orderClass.getModifiers()));
    }

    @Test
    public void testGetIdShouldReturnCorrectly() {
        Long id = order.getId();
        assertEquals(-1L, id);
    }

    @Test
    public void testGetCreateDateShouldReturnCorrectly() {
        Date createDate = order.getCreateDate();
        assertEquals(date, createDate);
    }

    @Test
    public void testGetUserIdShouldReturnCorrectly() {
        Long userId = order.getUserId();
        assertEquals(0L, userId);
    }

    @Test
    public void testGetSenderAddressShouldReturnCorrectly() {
        Address senderAddressDummy = order.getSenderAddress();
        assertEquals(senderAddress, senderAddressDummy);
    }

    @Test
    public void testGetReceiverAddressShouldReturnCorrectly() {
        Address receiverAddressDummy = order.getReceiverAddress();
        assertEquals(receiverAddress, receiverAddressDummy);
    }

    @Test
    public void testGetReceiverNameShouldReturnCorrectly() {
        assertEquals("Dummy", order.getReceiverName());
    }

    @Test
    public void testGetReceiverPhoneNumberShouldReturnCorrectly() {
        assertEquals("081234567890", order.getReceiverPhoneNumber());
    }

    @Test
    public void testGetTypeShouldReturnCorrectly() {
        assertEquals("Pakaian", order.getType());
    }

    @Test
    public void testGetWeightShouldReturnCorrectly() {
        assertEquals(1000L, order.getWeight());
    }

    @Test
    public void testGetLengthShouldReturnCorrectly() {
        assertEquals(100L, order.getLength());
    }

    @Test
    public void testGetWidthShouldReturnCorrectly() {
        assertEquals(50L, order.getWidth());
    }

    @Test
    public void testGetHeightShouldReturnCorrectly() {
        assertEquals(20L, order.getHeight());
    }

    @Test
    public void testGetCostShouldReturnCorrectly() {
        assertEquals(100000L, order.getCost());
    }

    @Test
    public void testGetPayedShouldReturnCorrectly() {
        assertFalse(order.isPayed());
    }

    @Test
    public void testSetIdShouldChangeId() {
        order.setId(0L);
        Long id = order.getId();
        assertEquals(0L, id);
    }

    @Test
    public void testSetUserIdShouldReturnCorrectly() {
        order.setUserId(-1L);
        assertEquals(-1L, order.getUserId());
    }

    @Test
    public void testSetSenderAddressShouldReturnCorrectly() {
        Address senderAddressDummy = new Address();
        order.setSenderAddress(senderAddressDummy);
        assertEquals(senderAddressDummy, order.getSenderAddress());
    }

    @Test
    public void testSetReceiverAddressShouldReturnCorrectly() {
        Address receiverAddressDummy = new Address();
        order.setReceiverAddress(receiverAddressDummy);
        assertEquals(receiverAddressDummy, order.getReceiverAddress());
    }

    @Test
    public void testSetReceiverNameShouldReturnCorrectly() {
        order.setReceiverName("DummyDummy");
        assertEquals("DummyDummy", order.getReceiverName());
    }

    @Test
    public void testSetReceiverPhoneNumberShouldReturnCorrectly() {
        order.setReceiverPhoneNumber("081234567809");
        assertEquals("081234567809", order.getReceiverPhoneNumber());
    }

    @Test
    public void testSetTypeShouldReturnCorrectly() {
        order.setType("Makanan");
        assertEquals("Makanan", order.getType());
    }

    @Test
    public void testSetWeightShouldReturnCorrectly() {
        order.setWeight(1010L);
        assertEquals(1010L, order.getWeight());
    }

    @Test
    public void testSetLengthShouldReturnCorrectly() {
        order.setLength(110L);
        assertEquals(110L, order.getLength());
    }

    @Test
    public void testSetWidthShouldReturnCorrectly() {
        order.setWidth(60L);
        assertEquals(60L, order.getWidth());
    }

    @Test
    public void testSetHeightShouldReturnCorrectly() {
        order.setHeight(30L);
        assertEquals(30L, order.getHeight());
    }

    @Test
    public void testSetCostShouldReturnCorrectly() {
        order.setCost(50000L);
        assertEquals(50000L, order.getCost());
    }

    @Test
    public void testSetPayedShouldReturnCorrectly() {
        order.setPayed(true);
        assertTrue(order.isPayed());
    }

}
