package id.ac.ui.cs.advrog.kureer.core;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.Collection;

public class AddressTest {

    private Class<?> addressClass;

    private Address address;

    @BeforeEach
    public void setUp() throws Exception {
        addressClass = Class.forName("id.ac.ui.cs.advrog.kureer.core.Address");
        address = new Address("dummyAlamat", "dummyDesaKelurahan", "dummyKecamatan",
                "dummyKotaKabupaten", "dummyProvinsi", "dummyKodePos");
    }

    @Test
    public void testAddressIsConcreteClass() {
        assertFalse(Modifier.isAbstract(addressClass.getModifiers()));
    }

    @Test
    public void testGetIdShouldReturnCorrectly() throws Exception {
        Long id = address.getId();
        assertNull(id);
    }

    @Test
    public void testGetAlamatShouldReturnCorrectly() throws Exception {
        String alamat = address.getAlamat();
        assertEquals("dummyAlamat", alamat);
    }

    @Test
    public void testGetDesaKelurahanShouldReturnCorrectly() throws Exception {
        String desaKelurahan = address.getDesaKelurahan();
        assertEquals("dummyDesaKelurahan", desaKelurahan);
    }

    @Test
    public void testGetKecamatanShouldReturnCorrectly() throws Exception {
        String kecamatan = address.getKecamatan();
        assertEquals("dummyKecamatan", kecamatan);
    }

    @Test
    public void testGetKotaKabupatenShouldReturnCorrectly() throws Exception {
        String kotaKabupaten = address.getKotaKabupaten();
        assertEquals("dummyKotaKabupaten", kotaKabupaten);
    }

    @Test
    public void testGetProvinsiShouldReturnCorrectly() throws Exception {
        String provinsi = address.getProvinsi();
        assertEquals("dummyProvinsi", provinsi);
    }

    @Test
    public void testGeKodePosShouldReturnCorrectly() {
        String kodePos = address.getKodePos();
        assertEquals("dummyKodePos", kodePos);
    }

    @Test
    public void testSetIdShouldChangeId() {
        address.setId(0L);
        Long id = address.getId();
        assertEquals(0L, id);
    }

    @Test
    public void testSetAlamatShouldChangeAlamat() {
        address.setAlamat("alamat");
        String alamat = address.getAlamat();
        assertEquals("alamat", alamat);
    }

    @Test
    void testSetDesaKelurahanShouldChangeDesaKelurahan() {
        address.setDesaKelurahan("desaKelurahan");
        String desaKelurahan = address.getDesaKelurahan();
        assertEquals("desaKelurahan", desaKelurahan);
    }

    @Test
    void testSetKecamatanShouldChangeKecamatan() {
        address.setKecamatan("kecamatan");
        String kecamatan = address.getKecamatan();
        assertEquals("kecamatan", kecamatan);
    }

    @Test
    void testSetKotaKabupatenShouldChangeKotaKabupaten() {
        address.setKotaKabupaten("kotaKabupaten");
        String kotaKabupaten = address.getKotaKabupaten();
        assertEquals("kotaKabupaten", kotaKabupaten);
    }

    @Test
    void testSetProvinsiShouldChangeProvinsi() {
        address.setProvinsi("provinsi");
        String provinsi = address.getProvinsi();
        assertEquals("provinsi", provinsi);
    }

    @Test
    void testSetKodePosShouldChangeKodePos() {
        address.setKodePos("kodePos");
        String kodePos = address.getKodePos();
        assertEquals("kodePos", kodePos);
    }

}
